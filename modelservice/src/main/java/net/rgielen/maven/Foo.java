package net.rgielen.maven;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class Foo {

    public String nix;

    public String foo() {
        return "bar";
    }

    public String bar() {
        if (nix == null) {
            nix.length();
        }
        return "bar";
    }
}

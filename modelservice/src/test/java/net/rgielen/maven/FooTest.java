package net.rgielen.maven;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class FooTest {

    @Test
    public void testFoo() throws Exception {
        assertEquals("bar", new Foo().foo());
    }

}
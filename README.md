# GMJ Training - Workflow and Process Notes

## Markdown Quickguide / Cheat Sheet
<https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>

## Maven

### Sample settings.xml for local Nexus
See <http://books.sonatype.com/nexus-book/reference/config-maven.html>
```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <mirrors>
        <mirror>
            <id>nexus</id>
            <mirrorOf>*</mirrorOf>
            <url>http://localhost:9100/nexus/content/groups/public</url>
        </mirror>
    </mirrors>
    
    <servers>
        <server>
            <id>mynexus-snapshots</id>
            <username>admin</username>
            <password>geheim</password>
        </server>
        <server>
            <id>mynexus-releases</id>
            <username>admin</username>
            <password>geheim</password>
        </server>
    </servers>
    
    <profiles>
        <profile>
            <id>nexus</id>
            <!--Enable snapshots for the built in central repo to direct -->
            <!--all requests to nexus via the mirror -->
            <repositories>
                <repository>
                    <id>central</id>
                    <url>http://central</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>central</id>
                    <url>http://central</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </snapshots>
                </pluginRepository>
            </pluginRepositories>
        </profile>
    </profiles>
    <activeProfiles>
        <!--make the profile active all the time -->
        <activeProfile>nexus</activeProfile>
     </activeProfiles>
</settings>
```
### Plugins

#### Maven Versions Plugin
Fine tuning for POM and dependency versions, also suitable for specialized released processes / CD.
 - <http://www.mojohaus.org/versions-maven-plugin/>

#### Maven Cargo Plugin
Deploy Java EE WARs / EARs to nearly any running server / container.
 - <https://codehaus-cargo.github.io/cargo/Maven2+plugin.html>

## Jenkins

### Run Jenkins with custom port
```java -jar jenkins.war --httpPort=9000```

### Run Jenkins in Docker
``` docker run --rm -p 2222:2222 -p 8080:8080 -p 8081:8081 -p 9418:9418 -ti jenkins```

### Maven Specials
 - Maven Release Plugin
   - https://wiki.jenkins-ci.org/display/JENKINS/M2+Release+Plugin 
   - https://dzone.com/articles/running-maven-release-plugin

### Jenkins Pipelines and Workflows
 - [Pipeline Tutorial](https://github.com/jenkinsci/pipeline-plugin/blob/master/TUTORIAL.md) 
 - Pipeline Examples
   - <https://jenkins.io/doc/pipeline/examples/>
   - <https://github.com/jenkinsci/pipeline-examples/tree/master/pipeline-examples>
 - [Parametrized Builds General](https://wiki.jenkins-ci.org/display/JENKINS/Parameterized+Build)
 - [Creating a Parametrized Pipeline](https://support.cloudbees.com/hc/en-us/articles/219250317-Create-a-Parameterized-Pipeline-using-Template) 
 
#### Jenkins Workflow Demo
<https://github.com/jenkinsci/workflow-aggregator-plugin/blob/master/demo>


## Nexus

### Configure Nexus start options
See [Nexus Configuration Reference](https://books.sonatype.com/nexus-book/reference/confignx-sect-network.html)

### Docker Images
 - [Nexus 2](https://hub.docker.com/r/sonatype/nexus/)
   - <https://github.com/sonatype/docker-nexus>
 - [Nexus 3](https://hub.docker.com/r/sonatype/nexus3/)
   - <https://github.com/sonatype/docker-nexus3>
